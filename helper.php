<?php

class helper
{
    const RIJNDAEL_DECRYPT = 'decrypt';
    const RIJNDAEL_ENCRYPT = 'encrypt';
    const KEY = 11805139041657014554594305883136;
    /**
     * @var string
     */
    private static $key;

    /**
     * Decrypts a string.
     *
     * @param $string
     *
     * @return string
     */
    public static function decrypt($string)
    {
        self::$key = '11805139041657014554594305883136';

        $encrypt_method = "AES-256-CBC";
        $secret_key = self::$key;
        $secret_iv = self::$key;

        // hash
        $key = hash('sha256', $secret_key);
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);


        // iv - encrypt method AES-256-CBC expects 16 bytes
        return openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }

    public static function encrypt($string)
    {
        self::$key = '11805139041657014554594305883136';


        $encrypt_method = "AES-256-CBC";
        $secret_key = self::$key;
        $secret_iv = self::$key;

        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);

        return $output;
    }

}



