<?php

require_once 'helper.php';

$username = 'parkerj05';
$pwd = 'P8;kwMv5`_,$6@>!';
$host = '10.80.49.100';
$db = 'poseidon';
$options = [
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
];

$dsn = "mysql:host={$host};dbname={$db}";

$pdo = new PDO($dsn, $username, $pwd, $options);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = "INSERT INTO poseidon.user (createdTime, username, password, email, forename, surname, is_active, cookie_token,
                           lastLoginTime, isManager, userCookies, lastLoginIpAddress, user_api_key, country, team,
                           password_age, password_last_change_date, password_next_change_date, last_password_reset_time,
                           manager, department, start_date, tenure, user_dashboard)
VALUES (current_timestamp, :username, '123', :email, :forename, :surname, TRUE, NULL, current_timestamp, 0, 0, 0, :api_key,
        'UK', 0, 0,
        current_timestamp, current_timestamp, current_timestamp, :manager_id, :department_id, current_timestamp, 0, 0);";

try {
    $pdo->beginTransaction();
    $stmt = $pdo->prepare($sql);

    $row = 1;
    $values = [];
    if (($handle = fopen("book1.csv", "r")) !== false) {
        while (($data = fgetcsv($handle, 1000, ",")) !== false) {
            $num = count($data);
            $row++;
            $values = [
                'username' => $data[0] . '.' . $data[1],
                'email' => $data[2],
                'forename' => helper::encrypt($data[0]),
                'surname' => helper::encrypt($data[1]),
                'api_key' => uniqid('', true),
                'manager_id' => $data[7],
                'department_id' => $data[3],
            ];
            $stmt->execute($values);
            echo "Inserted record {$pdo->lastInsertId()} for {$data[0]} {$data[1]}" . PHP_EOL;
        }
        $pdo->commit();
        fclose($handle);
    }
} catch (PDOException $exception) {
    echo $exception->getMessage();
}